8x8-px Titlebar icons for Openbox 3 that look nice with small bitmap fonts. 
Copy into your Openbox theme's /openbox-3 directory to use.

![](IAPTnightmare.png)
